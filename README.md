### i3 Config File
Some improvements to my workstation, like support to ABNT2 keyboard in the
start (you can set a keybinding to others layouts too, i'll add this
later), Dual monitors native arrangement (i'll implement more effective
checking before calling xrandr in the future) etc.

### TO DO

- [ ] Keymap ABNT2 and US layouts to change easily the layouts
- [ ] Improve checking of dual monitors to only call xrandr when it's
appropriated
- [ ] Add all main precompiled slack packages that serves me to i3 into repo
to make my life easy
